import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms'; // <-- NgModel lives here
import {AppComponent} from './app.component';
import {HeroesComponent} from './heroes/heroes.component';
import {HeroDetailComponent} from './hero-detail/hero-detail.component';
import {HeroService} from './hero.service';
import {FilePreviewerComponent} from './file-previewer/file-previewer.component';
import {FilePreviewerModalComponent} from './file-previewer-modal/file-previewer-modal.component';
import {MessagesComponent} from './messages/messages.component';
import {MessageService} from './messages.service';
import {AppRoutingModule} from './/app-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {Location} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';

/**
 * Angular needs to know how the pieces of your application fit together and what other files and libraries the app requires. This information is called metadata
 *
 * Some of the metadata is in the @Component decorators that you added to your component classes. Other critical metadata is in @NgModule decorators.
 *
 * Every component must be declared in exactly one NgModule
 *
 */
@NgModule({
  declarations: [
    AppComponent,
    // Every component must be declared in exactly one NgModule
    HeroesComponent,
    HeroDetailComponent,
    FilePreviewerComponent,
    FilePreviewerModalComponent,
    MessagesComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: false}
    )
  ],

  /*You must provide the HeroService in the dependency injection system before Angular can inject it into the HeroesComponent,
  as you will do below.

  There are several ways to provide the HeroService: in the HeroesComponent, in the AppComponent, in the AppModule.
  Each option has pros and cons.

  The providers array tells Angular to create a single, shared instance of HeroService and inject into any class that asks for it.

  */

  providers: [HeroService, MessageService, Location],
  bootstrap: [AppComponent]
})
export class AppModule { }
