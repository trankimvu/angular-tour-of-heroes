import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

/**
 * Google Docs Viewer
 *
 *Only files under 25 MB can be previewed with the Google Drive viewer.

  Google Drive viewer helps you preview over 16 different file types, listed below:

  Image files (.JPEG, .PNG, .GIF, .TIFF, .BMP)
  Video files (WebM, .MPEG4, .3GPP, .MOV, .AVI, .MPEGPS, .WMV, .FLV)
  Text files (.TXT)
  Markup/Code (.CSS, .HTML, .PHP, .C, .CPP, .H, .HPP, .JS)
  Microsoft Word (.DOC and .DOCX)
  Microsoft Excel (.XLS and .XLSX)
  Microsoft PowerPoint (.PPT and .PPTX)
  Adobe Portable Document Format (.PDF)
  Apple Pages (.PAGES)
  Adobe Illustrator (.AI)
  Adobe Photoshop (.PSD)
  Tagged Image File Format (.TIFF)
  Autodesk AutoCad (.DXF)
  Scalable Vector Graphics (.SVG)
  PostScript (.EPS, .PS)
  TrueType (.TTF)
  XML Paper Specification (.XPS)
  Archive file types (.ZIP and .RAR)

 */
@Component({
  selector: 'app-file-previewer',
  templateUrl: './file-previewer.component.html',
  styleUrls: ['./file-previewer.component.scss']
})
export class FilePreviewerComponent implements OnInit {

  @Input() fileUrl = 'http://www.sample-videos.com/sql/Sample-SQL-File-50000rows.sql';
  // @Input() fileUrl = 'http://www.sample-videos.com/img/Sample-png-image-500kb.png';
  // @Input() fileUrl = 'http://www.sample-videos.com/audio/mp3/wave.mp3';
  // @Input() fileUrl = 'http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4';
  // @Input() fileUrl = 'http://www.sample-videos.com/video/flv/720/big_buck_bunny_720p_2mb.flv';
  // @Input() fileUrl = 'http://www.sample-videos.com/img/Sample-jpg-image-1mb.jpg';
  // @Input() fileUrl = 'http://www.sample-videos.com/ppt/Sample-PPT-File-1000kb.ppt';
  // @Input() fileUrl = 'http://www.sample-videos.com/text/Sample-text-file-1000kb.txt';
  // @Input() fileUrl = 'http://www.sample-videos.com/xls/Sample-Spreadsheet-1000-rows.xls';
  // @Input() fileUrl = 'https://www.tutorialspoint.com/angularjs/angularjs_tutorial.pdf';
  // @Input() fileUrl = 'http://infolab.stanford.edu/pub/papers/google.pdf';
  // @Input() fileUrl = 'http://ieee802.org/secmail/docIZSEwEqHFr.doc';

  constructor(public sanitizer: DomSanitizer){}

  /**
   * For Office File Viewer
   * @param {string} uri
   * @returns encoded uri {string}
   */
  encodeURIComponent(uri: string): string {
    return encodeURIComponent(uri);
  }

  ngOnInit() {

  }

}
