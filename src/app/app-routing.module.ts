import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HeroesComponent} from './heroes/heroes.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HeroDetailComponent} from './hero-detail/hero-detail.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/dashboard', pathMatch: 'full'
  },
  {
    path: 'dashboard', component: DashboardComponent
  },
  {
    path: 'heroes', component: HeroesComponent
  },
  {
    path: 'detail/:id', component: HeroDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      enableTracing: true,
      errorHandler: err => {
        console.log(`Error on Routing: ${JSON.stringify(err)}`);
      },
      onSameUrlNavigation: 'ignore'
    }
  )],
  exports: [RouterModule]
})
/**
 * An Angular best practice is to load and configure the router in a separate, top-level module that is dedicated to routing and imported by the root AppModule
 *
 * A typical Angular Route has two properties:

 path: a string that matches the URL in the browser address bar.
 component: the component that the router should create when navigating to this route.
 */
export class AppRoutingModule {
}
