import {Component, OnInit} from '@angular/core';
import {Hero} from '../hero';
import {HeroService} from '../hero.service';

/**
 * You always import the Component symbol from the Angular core library and annotate the component class with @Component.
 *
 * Components shouldn't fetch or save data directly and they certainly shouldn't knowingly present fake data.
 * They should focus on presenting data and delegate data access to a service.
 */
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];

//  selectedHero: Hero;

  // onSelect(hero: Hero): void {
  //   this.selectedHero = hero;
  // }

  constructor(private heroService: HeroService) {
  }

  /**
   * While you could call getHeroes() in the constructor, that's not the best practice.
   * Reserve the constructor for simple initialization such as wiring constructor parameters to properties. The constructor shouldn't do anything.
   * It certainly shouldn't call a function that makes HTTP requests to a remote server as a real data service would.
   */
  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    // this.heroes = this.heroService.getHeroesSynchronously();
    this.heroService.getHeroesAsynchronously().subscribe(heroes => {
      this.heroes = heroes;
    });
  }

}
