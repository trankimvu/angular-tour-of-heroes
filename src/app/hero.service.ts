import {Injectable} from '@angular/core';
import {Hero} from './hero';
import {HEROES} from './mock-heroes';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {MessageService} from './messages.service';

/**
  Services are a great way to share information among classes that don't know each other.
  You'll create a MessageService and inject it in two places:

  in HeroService which uses the service to send a message.
  in MessagesComponent which displays that message.

 The @Injectable() decorator tells Angular that this service MIGHT itself have injected dependencies.
 It doesn't have dependencies now but it will soon. Whether it does or it doesn't, it's good practice to keep the decorator.

*/

@Injectable()
export class HeroService {

  /**
   * Angular will inject the singleton MessageService into that property when it creates the HeroService.
   */
  constructor(private messageService: MessageService) {
  }

  /**
   * This function is not functioning as expected in real applications, where the results cannot be returned immediately,
   * As if the server could return heroes instantly or the browser could freeze the UI while it waited for the server's response.
   *
   * In reality, the browser will not block while the service waits.
   */
  getHeroesSynchronously(): Hero[] {
    return HEROES;
  }

  getHeroesAsynchronously(): Observable<Hero[]> {
    // Todo: send the message _after_ fetching the heroes
    this.messageService.add('HeroService: fetched heroes');
    return of(HEROES);
  }

  getHeroAsynchronously(id: number): Observable<Hero> {
    // Todo: send the message _after_ fetching the hero
    this.messageService.add(`HeroService: fetched hero id=${id}`);
    return of(HEROES.find(hero => hero.id === id));
  }

}
