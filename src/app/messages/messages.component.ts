import {Component, OnInit} from '@angular/core';
import {MessageService} from '../messages.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  /**
   * Angular only binds to public component properties.
   * @param {MessageService} messageService
   */
  constructor(public messageService: MessageService) {}

  ngOnInit() {
  }

}
